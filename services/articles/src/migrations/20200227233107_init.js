exports.up = function(knex) {
  return knex.schema.createTable('articles', function(table) {
    table
      .increments('id')
      .primary()
      .notNullable();
    table.string('title').notNullable();
    table.string('description').notNullable();
    table.text('body').notNullable();
    table
      .integer('author')
      .unsigned()
      .notNullable();
    table.timestamps(true, true);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('articles');
};
