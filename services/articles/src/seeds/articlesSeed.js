const faker = require('faker');

const getArticles = () => {
  return Array.from({length: 10}, () => {
    const date = faker.date
      .between(new Date(2010), new Date(2020))
      .toISOString();
    return {
      title: faker.lorem.sentence(),
      description: faker.lorem.sentences(2),
      body: faker.lorem.sentences(10),
      author: faker.random.number({min: 1, max: 5}),
      created_at: date,
      updated_at: date
    };
  });
};

exports.seed = async knex => {
  await knex('articles')
    .del()
    .catch(err => console.error(err));
  // eslint-disable-next-line no-undef
  return Promise.all(
    getArticles().map(article => knex('articles').insert(article))
  );
};
