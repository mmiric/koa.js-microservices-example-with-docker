const bent = require('bent');
const getJSON = bent('json');

module.exports = async (ctx, next) => {
  const {header} = ctx.request;

  ctx.assert(
    header && header.authorization,
    403,
    'Invalid authentication token.'
  );

  let user;

  user = await getJSON(
    'http://users:8080/api/auth/status',
    {},
    {
      authorization: header.authorization
    }
  ).catch(err => ctx.throw(err.statusCode, 'Access denied to the resource.'));

  ctx.state.user = user;

  return next();
};
