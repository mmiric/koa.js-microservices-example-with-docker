const Router = require('koa-router');

const router = new Router();

const articles = require('../controllers').articles;
const authMiddleware = require('../middleware/authMiddleware');

router.param('slug', articles.slug);

router.get('/articles', articles.getArticlesList);
router.get('/articles/:slug', articles.getSingleArticle);
router.post('/articles', authMiddleware, authMiddleware, articles.addArticle);
router.put('/articles/:slug', authMiddleware, articles.editArticle);
router.delete('/articles/:slug', authMiddleware, articles.deleteArticle);

module.exports = router.routes();
