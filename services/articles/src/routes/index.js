const Router = require('koa-router');

const router = new Router();
const api = new Router();

const articles = require('./articlesRouter');

api.use(articles);

router.use('/api', api.routes());

module.exports = router;
