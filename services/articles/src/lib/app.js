const Koa = require('koa');

const app = new Koa();

app.proxy = true;

require('../schemas')(app);

const helmet = require('koa-helmet');
const logger = require('koa-pino-logger');
const cors = require('kcors');
const bodyParser = require('koa-bodyparser');

const errorMiddleware = require('../middleware/errorMiddleware');
const routes = require('../routes');

if (process.env.NODE_ENV === 'development') {
  app.use(logger());
}

app.use(helmet());
app.use(
  cors({
    origin: '*',
    exposeHeaders: ['authorization'],
    credentials: true,
    allowMethods: ['GET', 'PUT', 'POST', 'DELETE'],
    allowHeaders: ['authorization', 'Content-Type'],
    keepHeadersOnError: true
  })
);

app.use(errorMiddleware);

app.use(
  bodyParser({
    enableTypes: ['json']
  })
);

app.use(routes.routes());
app.use(routes.allowedMethods());

app.on('error', err => {
  if (process.env.NODE_ENV !== 'test') {
    console.log(err);
  }
});

module.exports = app;
