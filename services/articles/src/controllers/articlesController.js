const db = require('../lib/db');

module.exports = {
  async slug(slug, ctx, next) {
    ctx.assert(slug, 404, 'Not Found.');

    const article = await db('articles')
      .where({id: slug})
      .first()
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.assert(article, 404, 'Resource does not exist.');

    ctx.params.article = article;

    await next();
  },

  async getArticlesList(ctx) {
    const articles = await db('articles')
      .select('*')
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.status = 200;
    ctx.body = articles;
  },

  async getSingleArticle(ctx) {
    let {article} = ctx.params;

    ctx.status = 200;
    ctx.body = article;
  },

  async addArticle(ctx) {
    const {title, description, body} = ctx.request.body;

    const options = {abortEarly: false};

    const author = ctx.state.user.id;

    let article = {title, description, body, author};

    article = await ctx.app.schemas.article
      .validate(article, options)
      .catch(err => ctx.throw(err.statusCode, err.message));

    await db('articles')
      .insert(article)
      .returning('id')
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.status = 201;
  },

  async editArticle(ctx) {
    const {title, description, body} = ctx.request.body;

    const options = {abortEarly: false};

    let article = Object.assign({}, ctx.params.article, {
      title,
      description,
      body
    });

    article = await ctx.app.schemas.article
      .validate(article, options)
      .catch(err => ctx.throw(err.statusCode, err.message));

    article.updated_at = new Date().toISOString();

    await db('articles')
      .where({id: article.id})
      .update(article)
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.status = 200;
  },

  async deleteArticle(ctx) {
    const {id} = ctx.params.article;

    await db('articles')
      .del()
      .where({id})
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.status = 200;
  }
};
