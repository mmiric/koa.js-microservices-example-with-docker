process.env.NODE_ENV = 'test';

const request = require('supertest');

const app = require('../lib/app');
const knex = require('../lib/db');

beforeEach(async () => {
  await knex.migrate.rollback();
  await knex.migrate.latest();
  await knex.seed.run();
});

afterEach(async () => {
  await knex.migrate.rollback();
});

afterAll(async done => {
  knex.destroy();
  done();
});
