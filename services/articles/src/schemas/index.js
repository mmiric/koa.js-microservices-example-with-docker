const article = require('./articleSchema');

module.exports = function(app) {
  app.schemas = {
    article
  };
};
