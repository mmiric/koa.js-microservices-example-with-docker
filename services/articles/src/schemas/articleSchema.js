const yup = require('yup');
const timeStampSchema = require('./timestampSchema');

const articleSchema = yup
  .object()
  .shape({
    id: yup.number(),

    title: yup
      .string()
      .required()
      .trim(),

    description: yup
      .string()
      .required()
      .trim(),

    body: yup
      .string()
      .required()
      .trim(),

    author: yup.number()
  })
  .noUnknown()
  .concat(timeStampSchema);

module.exports = articleSchema;
