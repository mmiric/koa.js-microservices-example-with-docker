const yup = require('yup');
const timeStampSchema = require('./timestampSchema');

const userSchema = yup
  .object()
  .shape({
    id: yup.number(),

    email: yup
      .string()
      .required()
      .email()
      .lowercase()
      .trim(),

    password: yup.string().when('$validatePassword', {
      is: true,
      then: yup
        .string()
        .required()
        .min(8)
        .max(30),
    }),

    username: yup
      .string()
      .required()
      .max(30)
      .default('')
      .trim(),

    image: yup
      .string()
      .url()
      .default('')
      .trim(),
  })
  .noUnknown()
  .concat(timeStampSchema);

module.exports = userSchema;
