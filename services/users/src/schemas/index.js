const user = require('./userSchema');

module.exports = function(app) {
  app.schemas = {
    user
  };
};
