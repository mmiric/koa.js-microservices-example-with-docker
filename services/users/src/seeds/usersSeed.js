const bcrypt = require('bcryptjs');
const faker = require('faker');

const users = [
  {
    name: 'test1'
  },
  {
    name: 'test2'
  },
  {
    name: 'test3'
  },
  {
    name: 'test4'
  },
  {
    name: 'test5'
  }
];

const getUsers = () => {
  return users.map(user => {
    return {
      email: user.email || `${user.name}@test.com`,
      username: user.name,
      password: bcrypt.hashSync('test12345', 10),
      image: faker.image.avatar(),
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString()
    };
  });
};

exports.seed = async knex => {
  await knex('users')
    .del()
    .catch(err => console.error(err));
  return knex('users').insert(getUsers());
};
