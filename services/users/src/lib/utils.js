const jwt = require('jwt-simple');

const encodeToken = (userId, tokenType) => {
  let date;

  if (tokenType === 'access') {
    date = new Date(new Date().getTime() + 900000).toISOString();
  } else {
    date = new Date(new Date().getTime() + 2592000000).toISOString();
  }

  const payload = {
    exp: date,
    iat: new Date().toISOString(),
    sub: userId
  };

  return jwt.encode(payload, 'secret');
};

const decodeToken = token => {
  let payload;

  payload = jwt.decode(token, 'secret');

  return payload.sub;
};

module.exports = {
  encodeToken,
  decodeToken
};
