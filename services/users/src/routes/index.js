const Router = require('koa-router');

const router = new Router();
const api = new Router();

const auth = require('./authRouter');
const users = require('./usersRouter');

api.use(auth);
api.use(users);

router.use('/api', api.routes());

module.exports = router;
