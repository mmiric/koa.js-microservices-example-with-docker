const Router = require('koa-router');

const router = new Router();

const users = require('../controllers').users;
const authMiddleware = require('../middleware/authMiddleware');

router.param('slug', users.slug);

router.get('/users', users.getUsersList);
router.get('/users/:slug', authMiddleware, users.getSingleUser);
router.post('/users', authMiddleware, users.addUser);
router.put('/users/:slug', authMiddleware, users.editUser);
router.delete('/users/:slug', authMiddleware, users.deleteUser);

module.exports = router.routes();
