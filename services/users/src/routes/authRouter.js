const Router = require('koa-router');

const router = new Router();

const auth = require('../controllers').auth;

router.post('/auth/register', auth.register);
router.post('/auth/login', auth.login);
router.post('/auth/refresh', auth.refresh);
router.get('/auth/status', auth.status);

module.exports = router.routes();
