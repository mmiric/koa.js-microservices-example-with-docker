process.env.NODE_ENV = 'test';

const request = require('supertest');

const app = require('../lib/app');
const knex = require('../lib/db');

beforeEach(async () => {
  await knex.migrate.rollback();
  await knex.migrate.latest();
  await knex.seed.run();
});

afterEach(async () => {
  await knex.migrate.rollback();
});

afterAll(async done => {
  knex.destroy();
  done();
});

describe('GET /api/users', () => {
  test('should return a users list', async () => {
    const ctx = await request(app.callback()).get('/api/users');
    expect(ctx.statusCode).toBe(200);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toHaveLength(5);
    expect(ctx.body[0]).toMatchObject({
      id: expect.any(Number),
      email: expect.any(String),
      username: expect.any(String),
      image: expect.any(String),
      created_at: expect.any(String),
      updated_at: expect.any(String)
    });
  });
});

describe('POST /api/users', () => {
  test('should add a new user', async () => {
    const response = await request(app.callback())
      .post('/api/auth/login')
      .send({
        email: 'test1@test.com',
        password: 'test12345'
      });
    const ctx = await request(app.callback())
      .post('/api/users')
      .set('Authorization', `Bearer ${response.body.access_token}`)
      .send({
        email: 'test6@test.com',
        password: 'test12345',
        username: 'test6'
      });
    expect(ctx.statusCode).toBe(201);
    expect(ctx.type).toMatch('text/plain');
  });
});

describe('GET /api/users/:slug', () => {
  test('should return a single user', async () => {
    const response = await request(app.callback())
      .post('/api/auth/login')
      .send({
        email: 'test1@test.com',
        password: 'test12345'
      });
    const ctx = await request(app.callback())
      .get('/api/users/1')
      .set('Authorization', `Bearer ${response.body.access_token}`)
      .send();
    expect(ctx.statusCode).toBe(200);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toMatchObject({
      id: expect.any(Number),
      email: expect.any(String),
      username: expect.any(String),
      image: expect.any(String),
      created_at: expect.any(String),
      updated_at: expect.any(String)
    });
  });
});

describe('PUT /api/users/:slug', () => {
  test('should update a single user', async () => {
    const response = await request(app.callback())
      .post('/api/auth/login')
      .send({
        email: 'test1@test.com',
        password: 'test12345'
      });
    const ctx = await request(app.callback())
      .put('/api/users/1')
      .set('Authorization', `Bearer ${response.body.access_token}`)
      .send({
        email: 'test7@test.com',
        password: 'test12345',
        username: 'test7'
      });
    expect(ctx.statusCode).toBe(200);
    expect(ctx.type).toMatch('text/plain');
  });

  test('should throw a unprocessable entity error', async () => {
    const ctx = await request(app.callback())
      .put('/api/users/1')
      .send({
        email: 'wrongemail',
        password: 'test12345',
        username: 'test7'
      });
    expect(ctx.statusCode).toBe(422);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toMatchObject({
      errors: expect.any(String)
    });
  });
});

describe('DELETE /api/users/:slug', () => {
  test('should delete a user', async () => {
    const response = await request(app.callback())
      .post('/api/auth/login')
      .send({
        email: 'test1@test.com',
        password: 'test12345'
      });
    const ctx = await request(app.callback())
      .delete('/api/users/1')
      .set('Authorization', `Bearer ${response.body.access_token}`)
      .send();
    expect(ctx.statusCode).toBe(200);
    expect(ctx.type).toMatch('text/plain');
  });
});
