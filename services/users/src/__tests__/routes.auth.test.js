process.env.NODE_ENV = 'test';

const request = require('supertest');

const app = require('../lib/app');
const knex = require('../lib/db');

beforeEach(async () => {
  await knex.migrate.rollback();
  await knex.migrate.latest();
  await knex.seed.run();
});

afterEach(async () => {
  await knex.migrate.rollback();
});

afterAll(async done => {
  knex.destroy();
  done();
});

describe('POST /api/auth/register', () => {
  test('should register a new user', async () => {
    const ctx = await request(app.callback())
      .post('/api/auth/register')
      .send({
        email: 'test7@test.com',
        password: 'test12345',
        username: 'test7'
      });
    expect(ctx.statusCode).toBe(201);
    expect(ctx.type).toMatch('text/plain');
  });

  test('should throw a unprocessable entity error', async () => {
    const ctx = await request(app.callback())
      .post('/api/auth/register')
      .send({});
    expect(ctx.statusCode).toBe(422);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toMatchObject({
      errors: expect.any(String)
    });
  });

  test('should throw a bad request error', async () => {
    const ctx = await request(app.callback())
      .post('/api/auth/register')
      .send({
        email: 'test1@test.com',
        password: 'test12345',
        username: 'test1'
      });
    expect(ctx.statusCode).toBe(400);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toMatchObject({
      errors: expect.any(String)
    });
  });
});

describe('POST /api/auth/login', () => {
  test('should sign in a user and return auth tokens', async () => {
    const ctx = await request(app.callback())
      .post('/api/auth/login')
      .send({
        email: 'test1@test.com',
        password: 'test12345'
      });
    expect(ctx.statusCode).toBe(200);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toMatchObject({
      access_token: expect.any(String),
      refresh_token: expect.any(String)
    });
  });

  test('should throw a unprocessable entity error', async () => {
    const ctx = await request(app.callback())
      .post('/api/auth/login')
      .send({});
    expect(ctx.statusCode).toBe(422);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toMatchObject({
      errors: expect.any(String)
    });
  });

  test('should throw a authentication error', async () => {
    const ctx = await request(app.callback())
      .post('/api/auth/login')
      .send({
        email: 'test1@test.com',
        password: 'wrongpassword'
      });
    expect(ctx.statusCode).toBe(401);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toMatchObject({
      errors: expect.any(String)
    });
  });
});

describe('POST /api/auth/refresh', () => {
  test('should return auth tokens', async () => {
    const response = await request(app.callback())
      .post('/api/auth/login')
      .send({
        email: 'test1@test.com',
        password: 'test12345'
      });
    const ctx = await request(app.callback())
      .post('/api/auth/refresh')
      .send({
        refresh_token: response.body.refresh_token
      });
    expect(ctx.statusCode).toBe(200);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toMatchObject({
      access_token: expect.any(String),
      refresh_token: expect.any(String)
    });
  });

  test('should throw a authentication error', async () => {
    const ctx = await request(app.callback())
      .post('/api/auth/refresh')
      .send({
        refresh_token:
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOiIyMDIwLTA0LTA0VDAwOjU0OjQ5LjQwM1oiLCJpYXQiOiIyMDIwLTAzLTA1VDAwOjU0OjQ5LjQwM1oiLCJzdWIiOjd9.CUf5-V8Ckwy-1YFbLXsLbsnA0RiM9aHWuaxFPmboycU'
      });
    expect(ctx.statusCode).toBe(401);
    expect(ctx.type).toMatch('application/json');
    expect(ctx.body).toMatchObject({
      errors: expect.any(String)
    });
  });
});
