const app = require("../lib/app");

const PORT = process.env.PORT || 8080;

const server = app.listen(PORT, () => {
  console.log(`Webserver is ready and listening on port ${PORT}`);
});

// quit on ctrl-c when running docker in terminal
process.on('SIGINT', () => {
  console.info(
    'Got SIGINT (aka ctrl-c in docker). Graceful shutdown ',
    new Date().toISOString(),
  );
  shutdown();
});

// quit properly on docker stop
process.on('SIGTERM', () => {
  console.info(
    'Got SIGTERM (docker container stop). Graceful shutdown ',
    new Date().toISOString(),
  );
  shutdown();
});

let sockets = {},
  nextSocketId = 0;
server.on('connection', socket => {
  const socketId = nextSocketId++;
  sockets[socketId] = socket;

  socket.once('close', () => {
    delete sockets[socketId];
  });
});

// shut down server
const shutdown = () => {
  waitForSocketsToClose(10);

  server.close(err => {
    if (err) {
      console.error(err);
      process.exitCode = 1;
    }
    process.exit();
  });
};

const waitForSocketsToClose = counter => {
  if (counter > 0) {
    console.log(
      `Waiting ${counter} more ${
        counter === 1 ? 'seconds' : 'second'
      } for all connections to close...`,
    );
    return setTimeout(waitForSocketsToClose, 1000, counter - 1);
  }

  console.log('Forcing all connections to close now');
  for (let socketId in sockets) {
    sockets[socketId].destroy();
  }
};
