const bcrypt = require('bcryptjs');
const db = require('../lib/db');

module.exports = {
  async slug(slug, ctx, next) {
    ctx.assert(slug, 404, 'Not Found.');

    let user = await db('users')
      .where({id: slug})
      .first()
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.assert(user, 404, 'Resource does not exist.');

    delete user.password;

    ctx.params.user = user;

    await next();
  },

  async getUsersList(ctx) {
    const users = await db('users')
      .select('*')
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.status = 200;
    ctx.body = users;
  },

  async getSingleUser(ctx) {
    let {user} = ctx.params;

    ctx.status = 200;
    ctx.body = user;
  },

  async addUser(ctx) {
    const {username, email, password} = ctx.request.body;

    const options = {abortEarly: false, context: {validatePassword: true}};

    let user = {username, email, password};

    user = await ctx.app.schemas.user
      .validate(user, options)
      .catch(err => ctx.throw(err.statusCode, err.message));
    ž;

    user.password = await bcrypt
      .hash(password, 10)
      .catch(err => ctx.throw(err.statusCode, err.message));

    await db('users')
      .insert(user)
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.status = 201;
  },

  async editUser(ctx) {
    const {username, email, password} = ctx.request.body;

    const options = {abortEarly: false, context: {validatePassword: false}};

    if (password) {
      options.context.validatePassword = true;
    }

    let user = Object.assign({}, ctx.params.user, {username, email, password});

    user = await ctx.app.schemas.user
      .validate(user, options)
      .catch(err => ctx.throw(err.statusCode, err.message));

    if (password) {
      user.password = await bcrypt
        .hash(password, 10)
        .catch(err => ctx.throw(err.statusCode, err.message));
    }

    user.updated_at = new Date().toISOString();

    await db('users')
      .where({id: user.id})
      .update(user)
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.status = 200;
  },

  async deleteUser(ctx) {
    const {id} = ctx.params.user;

    await db('users')
      .del()
      .where({id})
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.status = 200;
  }
};
