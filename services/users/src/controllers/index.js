const auth = require('./authController');
const users = require('./usersController');

module.exports = {
  auth,
  users
};
