const bcrypt = require('bcryptjs');
const { encodeToken, decodeToken } = require('../lib/utils');
const db = require('../lib/db');

module.exports = {
  async register(ctx) {
    const { username, email, password } = ctx.request.body;

    ctx.assert(username && email && password, 422, 'Request is malformed.');

    let user = await db('users')
      .where({ email })
      .first()
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.assert(!user, 400, 'Sorry. That email already exists.');

    const options = {
      abortEarly: false,
      context: { validatePassword: true }
    };

    user = { username, email, password };

    user = await ctx.app.schemas.user
      .validate(user, options)
      .catch(err => ctx.throw(err.statusCode, err.message));

    user.password = await bcrypt
      .hash(password, 10)
      .catch(err => ctx.throw(err.statusCode, err.message));

    await db('users')
      .insert(user)
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.status = 201;
  },

  async login(ctx) {
    let { email, password } = ctx.request.body;

    ctx.assert(email && password, 422, 'Request is malformed.');

    const user = await db('users')
      .where({ email })
      .first()
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.assert(user, 401, 'invalid email or password.');

    const isValid = await bcrypt.compare(password, user.password);

    ctx.assert(isValid, 401, 'Enter a valid password.');

    const tokens = {
      access_token: encodeToken(user.id, 'access'),
      refresh_token: encodeToken(user.id, 'refresh')
    };

    ctx.status = 200;
    ctx.body = tokens;
  },

  async refresh(ctx) {
    const { refresh_token } = ctx.request.body;

    ctx.assert(refresh_token, 422, 'Request is malformed.');

    const id = decodeToken(refresh_token);

    const user = await db('users')
      .where({ id })
      .first()
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.assert(user, 401, 'Resource does not exist.');

    const tokens = {
      access_token: encodeToken(user.id, 'access'),
      refresh_token: encodeToken(user.id, 'refresh')
    };

    ctx.status = 200;
    ctx.body = tokens;
  },

  async status(ctx) {
    const { header } = ctx.request;

    ctx.assert(header.authorization, 403, 'Token required.');

    const access_token = header.authorization.split(' ')[1];

    const id = decodeToken(access_token);

    let user = await db('users')
      .where({ id })
      .first()
      .catch(err => ctx.throw(err.statusCode, err.message));

    ctx.assert(user, 401, 'Invalid token.');

    delete user.password;

    ctx.status = 200;
    ctx.body = user;
  }
};
