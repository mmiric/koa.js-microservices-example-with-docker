const db = require('../lib/db');
const {decodeToken} = require('../lib/utils');

module.exports = async (ctx, next) => {
  const {header} = ctx.request;

  ctx.assert(
    header && header.authorization,
    403,
    'Invalid authentication token.'
  );

  const access_token = header.authorization.split(' ')[1];

  let id = decodeToken(access_token);

  let user = await db('users')
    .where({id})
    .first()
    .catch(err => ctx.throw(err.statusCode, err.message));

  ctx.assert(user, 401, 'Invalid token.');

  delete user.password;

  ctx.state.user = user;

  return next();
};
