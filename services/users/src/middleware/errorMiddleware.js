module.exports = async (ctx, next) => {
  try {
    await next();
    ctx.assert(Number(ctx.response.status) !== 404, 404, 'Not Found');
  } catch (err) {
    ctx.type = 'application/json';

    const status = err.status || err.statusCode || 500;

    if (!ctx.response.body) {
      ctx.response.body = {error: {}};
    }

    ctx.body.error = err.message;
    ctx.status = status;

    ctx.app.emit('error', err, ctx);
  }
};
