#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z users-db 5432; do
  sleep 0.1
done

echo "PostgreSQL started"

knex migrate:latest --env development --knexfile ./knexfile.js
knex seed:run --env development --knexfile ./knexfile.js

./node_modules/.bin/nodemon -L ./src/bin/www